# MeFitMiniCaseReact
This is an ASP.NET Core Web Application. It is a RESTful API including end points for an exercise plan profile, management of the data and a frontend application.   

## Functionalities
### Frontend
* A home page with the following options (all linking to separate pages):
  * View existing user profiles 
  * Create a new profile
  * Add a new goal 
  * View, create, update and delete exercises

### Backend
* Provides Create, Read, Update and Delete (CRUD) endpoints for:
  * The profile class
  * The exercise class
  * The goal class
  * The program class
  * The workout class
* Persists data using Entity Framework in an exercise management app.

## Frontend application
This part of the application uses the React framework to create a single page application for managing an excercise plan.

### Components (*Dumb components*)
The application contanins the following components.

#### HomeComponent
Component displaying the homepage. Containing links to the other functionalities as described under [Frontend => Functionalities](#frontend).

#### AddProfileComponent
A component displaying an empty form allowing the user to submit a new profile to the API-server.

#### UpdateProfileComponent 
A component displaying a form filled in with the information on an existing profile (exept the password). Allows the user to update the information and submit the updates to the API-server.

#### ExerciseComponent
Displaying a form for submiting a new goal, and a list of all existing exercises below. The list includes buttons to delete or edit the exercises. The edit button redirects to the [UpdateExerciseContainer](#updateexercisecontainer).

#### UpdateExerciseComponent
Displaying a form filled in with the information on an existing exercise. Allows the user to update the information and submit the updates to the API-server. Includes a button redirecting to the [ExerciseContainer](#exercisecontainer).

#### GoalComponent
Displaying a form for submiting a new goal, and a list of all existing goals below.

#### NavMenu
A component displaying a nvigation bar at the to of a page.

#### FootMenu
A component diaplaying a foot menu on the bottom of a page.

#### Layout
A component wrapping all routes in this (single page) application displaying NavMenu component on the top of the page and a FootMenu on the bottom.

### Containers (*Smart components*)
The application contains the following containers.

#### HomeContainer
Entry point for the [HomeComponent](#homecomponent), and controls a call to the API-server to GET all profiles.

#### AddProfileContainer
Entry point for the [AddProfileComponent](#addprofilecomponent). Controls a call to the API-server to GET all workouts, programs and goals, and to POST new profiles. After a new profile is posted to the server a button is displayed routing to the UpdateProfileContainer of the new profile just posted.

#### UpdateProfileContainer
Entry point for the [UpdateProfileComponent](#updateprofilecomponent). Controls a call to the API-server to GET all workouts, programs and goals, and the request to GET the profile in question. It also controles the request to PUT the updated profile. After a profile is updated it alerts the user that the profile is submitted, and stays on the same page.

#### ExerciseContainer
Entry point for the [ExerciseComponent](#exercisecomponent). Controls a call to the API-server to GET all exercises, and to POST new exercises. After a new exercise is posted it reloads the page such that the new exercise is displayed in the list in the ExerciseComponent.

#### UpdateExerciseContainer
Entry point for the [UpdateExerciseComponent](#updateexercisecomponent). Controls a call to the API-server to GET the exercise in question, and to PUT the updated exercise. After an exercise is updated it alerts the user that the exercise is submitted, and stays on the same page.

#### GoalContainer
Entry point for the [GoalComponent](#goalcomponent). Controls a call to the API-server to GET all goals, and to POST new goals. After a goal is submitted it alerts the user that the goal is submitted and reloads pagesuch that the new exercise is displayed in the list in the GoalComponent.

## Backend application
### Database diagram
![Database diagram](/MeFitMiniCaseReact/images/db_diagram.jpg "Logo Title Text 1")

### Controllers 
- **ProfilesController**
The main controller. Controles communication with the database which relates to the profile model. GET requests return address and user information.

- **AddressController**
Controles communication with the database which relates to the address model
Automatically built skeleton, GET requests returns only pure address information. 

- **ProgramsController**
Controles communication with the database which relates to the program model
Automatically built skeleton, GET requests returns only pure program information.

- **WorkoutsController**
Controles communication with the database which relates to the workout model
Automatically built skeleton, GET requests returns only pure workout information.


### Models
#### Profile
Model for managing information on a profile. Holds the following data:
  * Medical conditions
  * Disabilities { get; set; }
  * Weight (in kg)
  * Height (in cm)

In addition it holds references to the following other models:
  * Address (one-to-one)
  * User (one-to-one)
  * Goal (one-to-many)
  * Program (one-to-many)
  * Workout (one-to-many)

#### Address
Model for managing information about an address. Holds the following data:
  * AddressLine 1
  * AddressLine 2
  * AddressLine 3
  * Postal code
  * City
  * Country
In addition it holds references to the following other models:
  * Profile (one-to-one)

#### User
Model for managing information about a user. Holds the following data:
  * Password
  * First name
  * Last name
  * If the user is an admin
  * If the user is an contributor

In addition it holds references to the following other models:
  * Profile (one-to-one)

#### Exercise
Model for managing information about an exercise. Holds the following data:
  * Name
  * Description
  * Target muscle group
  * Image location
  * Video link

In addition it holds references to the following other models:
  * Set (one-to-many)

#### Goal
Model for managing information about a goal. Holds the following data:
  * End date
  * Information on wether it is achieved or not

In addition it holds references to the following other models:
  * Workout (many-to-many)
  * Profile (one-to-many)

#### GoalWorkout
Model for managing the many to many relationship between the goal and workout models.

#### Workout
Model for managing information about a workout. Holds the following data:
  * Name
  * Type
  * Information on wether it is complete or not

In addition it holds references to the following other models:
  * Set (many-to-many)
  * Goal (many-to-many)
  * Program (many-to-many)
  * Profile (one-to-many)

#### Program
Model for managing information about a workout. Holds the following data:
  * Name
  * Category

In addition it holds references to the following other models:
  * Workout (many-to-many)

#### ProgramWorkout
Model for managing the many to many relationship between the program and workout models.


#### Set
Model for managing information about a workout. Holds the following data:
  * Exercise repetitions
  * Number of sets

In addition it holds references to the following other models:
  * Workout (many-to-many)
  * Exercise (one-to-many)
  * Profile (one-to-many)


#### SetWorkout
Model for managing the many to many relationship between the set and workout models.


## Author
**Ole Baadshaug**

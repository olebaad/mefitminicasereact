﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MeFitMiniCaseReact.Data;
using MeFitMiniCaseReact.Models;

namespace MeFitMiniCaseReact.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfilesController : ControllerBase
    {
        private readonly MeFitDbContext _context;

        public ProfilesController(MeFitDbContext context)
        {
            _context = context;
        }

        // GET: api/Profiles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Profile>>> GetProfiles()
        {
            var profiles = await _context.Profiles
                .Include(s => s.User)
                .Include(s => s.Address)
                .Include(s => s.Workout)
                .Include(s => s.Program)
                .Include(s => s.Goal)
                .ToListAsync();
            return profiles;
        }

        // GET: api/Profiles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Profile>> GetProfile(int id)
        {
            Profile profile;

            try
            {
                profile = await _context.Profiles
                .Include(s => s.User)
                .Include(s => s.Address)
                .Include(s => s.Workout)
                .Include(s => s.Program)
                .Include(s => s.Goal)
                .SingleOrDefaultAsync(s => s.Id == id);
            }
            catch (Exception e)
            {
                Exception e2 = (Exception)Activator.CreateInstance(e.GetType(), "Request to profile database failed.", e);
                throw e2;
            }

            if (profile == null)
            {
                return NotFound();
            }

            return profile;
        }

        // GET: api/Profiles/5/Address
        [HttpGet("{id}/Address")]
        public async Task<ActionResult<Address>> GetAddress(int id)
        {
            var profile = await _context.Profiles.Where(s => s.Id == id).Include(s => s.Address).SingleAsync();

            if (profile == null)
            {
                return NotFound();
            }

            return profile.Address;
        }
        // PUT: api/Profiles/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<ActionResult<Profile>> PutProfile(int id, Profile profile)
        {
            if (id != profile.Id)
            {
                return BadRequest();
            }

            _context.Entry(profile).State = EntityState.Modified;
            _context.Entry(profile.User).State = EntityState.Modified;
            _context.Entry(profile.Address).State = EntityState.Modified;


            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProfileExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return profile;
        }

        // POST: api/Profiles
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Profile>> PostProfile(Profile profile)
        {
            _context.Profiles.Add(profile);
            _context.Users.Add(profile.User);
            _context.Addresses.Add(profile.Address);

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProfile", new { id = profile.Id }, profile);
        }


        // DELETE: api/Profiles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Profile>> DeleteProfile(int id)
        {
            var profile = await _context.Profiles.FindAsync(id);
            if (profile == null)
            {
                return NotFound();
            }

            _context.Profiles.Remove(profile);
            _context.Users.Remove(profile.User);
            _context.Addresses.Remove(profile.Address);
            await _context.SaveChangesAsync();

            return profile;
        }

        private bool ProfileExists(int id)
        {
            return _context.Profiles.Any(e => e.Id == id);
        }
    }
}

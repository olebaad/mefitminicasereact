﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MeFitMiniCaseReact.Models
{
    public class Workout
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public bool Complete { get; set; }

        private ICollection<Profile> Profiles { get; set; }

        public ICollection<ProgramWorkout> ProgramWorkouts { get; set; }

        public ICollection<GoalWorkout> GoalWorkouts { get; set; }

        public ICollection<SetWorkout> SetWorkouts { get; set; }
    }

}

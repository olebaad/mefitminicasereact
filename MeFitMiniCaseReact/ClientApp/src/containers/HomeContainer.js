import React, { Component } from 'react';
import HomeComponent from '../components/HomeComponent.js';

export class HomeContainer extends Component {

  state = {
    uri: "https://localhost:44363/api/profiles/",
    isLoading: true,
    profiles: undefined,
    searchResult: [],
  }

  async componentDidMount() {
    await this.loadProfiles(this.state.uri);
  }

  async loadProfiles(profileUri) {
    const req = new Request(profileUri, {
      method: 'GET',
    });

    await fetch(req).then( (response) => {
      return response.json();
    })
    .then( (data) => {
      this.setState( { 
        profiles: data,
        isLoading: false
      } );
    })
    .catch( err => console.log('ERROR in loadProfiles:', err) )
  }

  searchProfilesFunction(nameInput) {
    let searchRes = [];
    const name = nameInput.toLowerCase();
    
    searchRes = this.state.profiles.users.filter( user => {
      const currName = `${user.firstName} ${user.lastName}`
      return currName.toLowerCase().includes(name);
    });
    this.setState( {searchResult: searchRes} )
  }

  render() {
    const {isLoading, profiles} = this.state;
    return (
      <div>
        {
          !isLoading ? (
            <HomeComponent isLoading={isLoading} profiles={profiles}/>
          ) : (
            <h3>Loading....</h3>
          )
        }  
      </div>
    )
  }
}
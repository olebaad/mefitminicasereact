import React, { Component } from 'react';
import AddProfileComponent from '../components/AddProfileComponent';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export class AddProfileContainer extends Component {

  state = {
    uri: "https://localhost:44363/api/profiles",
    goalUri: "https://localhost:44363/api/goals",
    workoutUri: "https://localhost:44363/api/workouts",
    programUri: "https://localhost:44363/api/programs",
    notSubmittedYet: true,
    profile: undefined,
    goals: [],
    workouts: [],
    programs: [],
  }

  async componentDidMount() {
    await this.loadWorkouts(this.state.workoutUri);
    await this.loadGoals(this.state.goalUri);
    await this.loadPrograms(this.state.programUri);
  }

  async loadGoals(goalsUri) {
    const req = new Request(goalsUri, {
      method: 'GET',
    });

    await fetch(req)
      .then( (response) => {
        return response.json();
      })
      .then( (data) => {
        this.setState( { 
          goals: data,
        } );
      })
      .catch( err => alert('ERROR in loadGoals:', err) )
  }
  
  async loadWorkouts(workoutUri) {
    const req = new Request(workoutUri, {
      method: 'GET',
    });

    await fetch(req)
      .then( (response) => {
        return response.json();
      })
      .then( (data) => {
        this.setState( { 
          workouts: data,
        } );
      })
      .catch( err => alert('ERROR in loadWorkouts:', err) )
  }

  async loadPrograms(programsUri) {
    const req = new Request(programsUri, {
      method: 'GET',
    });

    await fetch(req)
      .then( (response) => {
        return response.json();
      })
      .then( (data) => {
        this.setState( { 
          programs: data,
        } );
      })
      .catch( err => alert('ERROR in loadPrograms:', err) )
  }

  profileSubmit = (profile) => {
    
    fetch(this.state.uri, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(profile)
    })
    .then(response => response.json())
    .then(data => {
      console.log('Successfully added: ', data.user.firstName)
      this.setState( { 
        profile: data,
        notSubmittedYet: false
      } );
    })
    .catch(err => {
      console.log('Error: ', err) 
    })
  }

  render() {

    const {notSubmittedYet, profile, programs, goals, workouts} = this.state;
    
    return (
      notSubmittedYet ? (
        <div>      
          <AddProfileComponent 
            profileSubmit={this.profileSubmit} 
            programs={programs} workouts={workouts} 
            goals={goals} 
          />
        </div>
      ) : (
        <div>
          <h3>Profile created</h3>
          <p>Click button to view and update profile.</p>
          <Link to={'/profile/' + profile.id}><Button>View Profile</Button></Link>
        </div> 
      )
      
    )
  }
}
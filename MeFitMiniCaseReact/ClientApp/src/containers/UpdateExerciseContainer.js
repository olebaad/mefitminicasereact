import React, { Component } from 'react';
import UpdateExerciseComponent from '../components/UpdateExerciseComponent';

export class UpdateExerciseContainer extends Component {

  state = {
    uri: "https://localhost:44363/api/exercises/",
    exercise: {}
  }

  async componentDidMount() {
    const { id } = this.props.match.params;
    const exerciseUri = this.state.uri + id;

    await this.loadExercise(exerciseUri);
  }
  
  exerciseSubmit = (exercise) => {    
    fetch(this.state.uri + this.state.exercise.id, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(exercise)
    })
    .then(response => response.json())
    .then(data => {
      alert(`${data.name} updated!`)
    })
    .catch(err => {
      console.log('Error in exerciseSubmit: ', err) 
    })
  }

  async loadExercise(exerciseUri) {
    const req = new Request(exerciseUri, {
      method: 'GET',
    });

    await fetch(req)
      .then( (response) => {
        return response.json();
      })
      .then( (data) => {
        this.setState( { 
          exercise: data,
          isLoading: false
        } );
      })
      .catch( err => alert('ERROR in loadProfile:', err) )
  }

  render() {

    const {isLoading, exercise} = this.state;

    return (
      !isLoading ? (
        <div>      
          <UpdateExerciseComponent 
            key={exercise.id} 
            currentExercise={exercise} 
            exerciseSubmit={this.exerciseSubmit}
            />
        </div>
      ) : (
        <h3>Loading....</h3>
      )
      
    )
  }
}
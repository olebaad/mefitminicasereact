import React, { Component } from 'react';
import UpdateProfileComponent from '../components/UpdateProfileComponent';

export class UpdateProfileContainer extends Component {

  state = {
    profilesUri: "https://localhost:44363/api/profiles/",
    goalUri: "https://localhost:44363/api/goals",
    workoutUri: "https://localhost:44363/api/workouts",
    programUri: "https://localhost:44363/api/programs",
    isLoading: true,
    profile: undefined,
    programs: [],
    workouts: [],
    goals: []
  }

  profileSubmit = (profile) => {    
    fetch(this.state.profilesUri + this.state.profile.id, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(profile)
    })
    .then(response => response.json())
    .then(data => {
      alert("Profile submitted!")
    })
    .catch(err => {
      console.log('Error: ', err) 
    })
  }

  async componentDidMount() {
    const { id } = this.props.match.params;
    const profileUri = this.state.profilesUri + id;
    
    await this.loadWorkouts(this.state.workoutUri);
    await this.loadGoals(this.state.goalUri);
    await this.loadPrograms(this.state.programUri);
    await this.loadProfile(profileUri);
  }

  async loadProfile(profileUri) {
    const req = new Request(profileUri, {
      method: 'GET',
    });

    await fetch(req)
      .then( (response) => {
        return response.json();
      })
      .then( (data) => {
        this.setState( { 
          profile: data,
          isLoading: false
        } );
      })
      .catch( err => alert('ERROR in loadProfile:', err) )
  }
  async loadGoals(goalsUri) {
    const req = new Request(goalsUri, {
      method: 'GET',
    });

    await fetch(req)
      .then( (response) => {
        return response.json();
      })
      .then( (data) => {
        this.setState( { 
          goals: data,
        } );
      })
      .catch( err => alert('ERROR in loadGoals:', err) )
  }
  
  async loadWorkouts(workoutUri) {
    const req = new Request(workoutUri, {
      method: 'GET',
    });

    await fetch(req)
      .then( (response) => {
        return response.json();
      })
      .then( (data) => {
        this.setState( { 
          workouts: data,
        } );
      })
      .catch( err => alert('ERROR in loadWorkouts:', err) )
  }

  async loadPrograms(programsUri) {
    const req = new Request(programsUri, {
      method: 'GET',
    });

    await fetch(req)
      .then( (response) => {
        return response.json();
      })
      .then( (data) => {
        this.setState( { 
          programs: data,
        } );
      })
      .catch( err => alert('ERROR in loadPrograms:', err) )
  }

  render() {

    const {isLoading, profile, programs, workouts, goals} = this.state;

    return (
      !isLoading ? (
        <div>      
          <UpdateProfileComponent 
            key={profile.id} 
            currentProfile={profile} 
            programs={programs} 
            workouts={workouts}
            goals={goals}
            profileSubmit={this.profileSubmit}
            />
        </div>
      ) : (
        <h3>Loading....</h3>
      )
      
    )
  }
}
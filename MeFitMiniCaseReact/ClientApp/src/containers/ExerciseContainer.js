import React, { Component } from 'react';
import ExerciseComponent from '../components/ExerciseComponent';

export class ExerciseContainer extends Component {

  state = {
    uri: "https://localhost:44363/api/exercises",
    exercises: []
  }

  async componentDidMount() {
    await this.loadExercises(this.state.uri);
  }

  async loadExercises(exercisesUri) {
    const req = new Request(exercisesUri, {
      method: 'GET',    
    });

    fetch(req)
    .then( (response) => {
      return response.json();
    })
    .then( (data) => {
      this.setState( { 
        exercises: data,
      } );
    })
    .catch( err => console.log('ERROR in loadExercises:', err) )
  }

  exerciseSubmit = (exercise) => {
    
    fetch(this.state.uri, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(exercise)
    })
    .then(response => response.json())
    .then(data => {
      window.location.reload(false);
    })
    .catch(err => {
      console.log('Error: ', err) 
    })
  }

  exerciseDelete = (id) => {
    const req = new Request(`${this.state.uri}/${id}`, {
      method: 'DELETE',    
    });

    fetch(req)
    .then( (response) => {
      return response.json();
    })
    .then( (data) => {
      window.location.reload(false)
    })
    .catch( err => console.log('ERROR in deleteExercises:', err) )
  }
  
  render() {

    const {exercises} = this.state;
    
    return (
      <div>      
        <ExerciseComponent 
          exerciseSubmit={this.exerciseSubmit} 
          exercises={exercises} 
          exerciseDelete={this.exerciseDelete}
        />
      </div>      
    )
  }
}
import React, { Component } from 'react';
import GoalComponent from '../components/GoalComponent';

export class GoalContainer extends Component {

  state = {
    uri: "https://localhost:44363/api/goals",
    goals: [],
  }

  async componentDidMount() {
    await this.loadGoals(this.state.uri);
  }

  async loadGoals(goalsUri) {
    const req = new Request(goalsUri, {
      method: 'GET',    
    });

    await fetch(req)
    .then( (response) => {
      return response.json();
    })
    .then( (data) => {
      this.setState( { 
        goals: data,
      } );
    })
    .catch( err => console.log('ERROR in loadExercises:', err) )
  }

  goalSubmit = (goal) => {
    
    fetch(this.state.uri, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(goal)
    })
    .then(response => response.json())
    .then(data => {
      alert(`${data.type} submitted successfully!`)
      window.location.reload(false)
    })
    .catch(err => {
      console.log('Error: ', err) 
    })
  }

  render() {    
    return (
      <div>      
        <GoalComponent goalSubmit={this.goalSubmit} goals={this.state.goals} />
      </div>      
    )
  }
}
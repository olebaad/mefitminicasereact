import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { HomeContainer } from './containers/HomeContainer';
import { UpdateProfileContainer } from './containers/UpdateProfileContainer';
import { AddProfileContainer } from './containers/AddProfileContainer';
import { ExerciseContainer } from './containers/ExerciseContainer';
import { GoalContainer } from './containers/GoalContainer';

import './custom.css'
import { UpdateExerciseContainer } from './containers/UpdateExerciseContainer';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={HomeContainer} />
        <Route path='/profile/:id' component={UpdateProfileContainer} />
        <Route path='/createprofile' component={AddProfileContainer} />
        <Route path='/exercises' component={ExerciseContainer} />
        <Route path='/goals' component={GoalContainer} />
        <Route path='/exercise/:id' component={UpdateExerciseContainer} />
      </Layout>
    );
  }
}

import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavItem, NavLink } from 'reactstrap';
import './NavMenu.css';

export class FootMenu extends Component {
  static displayName = FootMenu.name;

  constructor (props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true
    };
  }

  toggleNavbar () {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  render () {
    return (
      <footer>
        <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-white border-top box-shadow-bottom mt-5" light>
          <Container>
            <NavbarBrand>Other <span>(unrelated)</span> fun stuff</NavbarBrand>
            <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!this.state.collapsed} navbar>
              <ul className="navbar-nav flex-grow">
                <NavItem className="border-left">
                  <NavLink className="text-dark" href='https://rick-and-morty-and-the-dudes.herokuapp.com/'>Rick and Morty</NavLink>
                </NavItem>
                <NavItem className="border-left border-right">
                  <NavLink className="text-dark" href='https://pokemonapi-olebaad.herokuapp.com/pokemon'>PokèAPI</NavLink>
                </NavItem>
                <NavItem className="border-left border-right">
                  <NavLink className="text-dark" href='https://my-gitlab-user-card.herokuapp.com/'>Your GitLab user card</NavLink>
                </NavItem>
                <NavItem className="border-left border-right">
                  <NavLink className="text-dark" href='https://triviaole.herokuapp.com/#/'>A Trivia-quiz</NavLink>
                </NavItem>
              </ul>
            </Collapse>
          </Container>
        </Navbar>
      </footer>
    );
  }
}

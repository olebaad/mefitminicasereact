import React, { useState } from 'react';
import { Table} from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import { Button, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';

const ExerciseComponent = ( {exerciseSubmit, exercises, exerciseDelete} ) => {
  
  //Add an exercise object to state
  const [exercise, setExercise] = useState({})

  /**
   * Event handlers:
   */

  //Exercise form eventhandler
  const updateExercise = (event) => {
    setExercise({
      ...exercise,
      [event.target.name]: event.target.value,
    })
  }

  // Submit function
  const handleSubmit = (event) => {
    event.preventDefault();
    exerciseSubmit(exercise);
  }


  return (
    <div>
      <Form onSubmit={handleSubmit} className="pb-3 border-bottom">
        <Form.Group>
          <Row>
            <Col md={6}>
              <Form.Label>Exercise Name</Form.Label>
              <Form.Control 
                required
                type="text" 
                placeholder="Enter first name" 
                name="name" 
                onChange={updateExercise}
                maxLength= "20"
              />
            </Col>
            <Col md={6}>
              <Form.Label>Image link</Form.Label>
              <Form.Control 
                type="url" 
                placeholder="Link to a descriptive image" 
                name="imageLocation" 
                onChange={updateExercise}  
              />
            </Col>
          </Row>
          <Row>
          <Col md={6}>
              <Form.Label>Add the target muscle group</Form.Label>
              <Form.Control 
                required
                type="text" 
                placeholder="Muscle group" 
                name="targetMuscleGroup"  
                onChange={updateExercise}
                maxLength= "40"
              />
            </Col>
            <Col md={6}>
              <Form.Label>Video link</Form.Label>
              <Form.Control 
                type="url" 
                placeholder="Link to a descriptive image" 
                name="videoLink" 
                onChange={updateExercise}  
              />
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <Form.Label>Description</Form.Label>
              <Form.Control 
                required
                as="textarea"
                rows="3"
                onChange={updateExercise} 
                placeholder="Add an exercise description (max 140 caracters)"
                maxLength= "140"
                name="description"
              />
            </Col>
          </Row>
        </Form.Group>
        <Button color="primary" type="submit">Add Exercise</Button>
      </Form>
      <div className="pt-3">
        <h3>Existing exercises</h3>
        <Table striped bordered  className="border-right-0">
          <thead>
            <tr>
              <th>#</th>
              <th style={{width: '150px'}}>Name</th>
              <th>Description</th>
              <th style={{width: '120px'}}>Image Link</th>
              <th style={{width: '120px'}}>Video Link</th>
            </tr>
          </thead>
            <tbody>
            {
              exercises.map( (ex) => {
                return (
                  <tr key={ex.id}>
                    <td>
                        {`${ex.id}`}
                    </td>
                    <td>
                        {`${ex.name}`}
                    </td>
                    <td>
                        {`${ex.description}`}
                    </td>
                    {
                      ex.imageLocation ? (
                        <td>
                          <a href={ex.imageLocation}>Click here</a>
                        </td>
                      ) : (
                        <td>
                          No link
                        </td>
                      )
                    }
                    {
                      ex.videoLink ? (
                        <td>
                          <a href={ex.videoLink}>Click here</a>
                        </td>
                      )  : (
                        <td>
                          No link
                        </td>
                      )
                    }
                    <td className="border-0  bg-white">
                      <Button color="danger" onClick={() => exerciseDelete(ex.id)}>Delete</Button>
                    </td>
                    <td className="border-0  bg-white">
                      <Link to={`/exercise/${ex.id}`}>
                        <Button color="success">Edit</Button>
                      </Link>
                    </td>
                  </tr>
                );
              })
            }
          </tbody>
        </Table>
      </div>
    </div>
  );
}
export default ExerciseComponent;
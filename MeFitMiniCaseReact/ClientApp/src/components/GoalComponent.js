import React, { useState } from 'react';
import { Button, Table} from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import { Row, Col } from 'reactstrap';

const GoalComponent = ( {goalSubmit, goals} ) => {
  
  //Add an exercise object to state
  const [goal, setGoal] = useState({})

  const changeToDateTime = (event) => {
    event.target.type='datetime-local'
  }
  /**
   * Event handlers:
   */
     
  //Exercise form eventhandler
  const updateGoal = (event) => {
    let value = event.target.value;
    if(event.target.type === "radio" && event.target.checked) {
      value = JSON.parse(value)
    } 
    
    setGoal({
      ...goal,
      [event.target.name]: value,
    })
  }

  
  // Submit function
  const handleSubmit = (event) => {
    event.preventDefault();
    goalSubmit(goal)
  }


  return (
    <div>
      <Form onSubmit={handleSubmit} className="pb-3 border-bottom">
        <Form.Group>
          <Row className="pb-3">
            <Col md={3}>
              <Form.Label>Type</Form.Label>
              <Form.Control 
                required
                type="text" 
                placeholder="Goal type" 
                name="type" 
                onChange={updateGoal}
                maxLength= "20"
              />
            </Col>
            <Col md={3}>
              <Form.Label>End date</Form.Label>
              <Form.Control 
                required
                type="text" 
                placeholder="Enter planned end date"
                name="endDate" 
                onChange={updateGoal}  
                onFocus={changeToDateTime}
              />
            </Col>
            <Col className="border-left">
              <Form.Check>
                <Form.Label> <b>Is the achievement completed?</b></Form.Label>
                <Form.Check label="Yes" name="achieved" type="radio" value={true} onChange={updateGoal} />
                <Form.Check label="No" name="achieved" type="radio" value={false} onChange={updateGoal} defaultChecked={true} />
              </Form.Check>
            </Col>
          </Row>
        </Form.Group>
        <Button type="submit">Add Goal</Button>
      </Form>
      <div className="pt-3">
        <h3>Existing goals</h3>
        <Table striped bordered style={{width: '500px'}}>
          <thead>
            <tr>
              <th style={{width: '20px'}}>#</th>
              <th style={{width: '150px'}}>Type</th>
              <th >End date</th>
              <th style={{width: '120px'}}>Completed</th>
            </tr>
          </thead>
            <tbody>
            {
              goals.map( (g) => {
                const date = new Date(g.endDate).toDateString()
                return (
                  <tr key={g.id}>
                    <td>
                        {`${g.id}`}
                    </td>
                    <td>
                        {`${g.type}`}
                    </td>
                    <td>
                        {`${date}`}
                    </td>
                    {
                      g.achieved === false ? (
                        <td>No</td>
                      ) : (
                        <td>Yes</td>
                      )
                    }
                  </tr>
                );
              })
            }
          </tbody>
        </Table>
      </div>
    </div>
  );
}
export default GoalComponent;
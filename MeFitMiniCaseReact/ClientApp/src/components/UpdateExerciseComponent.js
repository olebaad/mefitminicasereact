import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import { Button, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';

const UpdateExerciseComponent = ( {exerciseSubmit, currentExercise} ) => {
  //Add an exercise object to state
  const [exercise, setExercise] = useState({
    id: currentExercise.id,
    name: currentExercise.name,
    imageLocation: currentExercise.imageLocation,
    targetMuscleGroup: currentExercise.targetMuscleGroup,
    videoLink: currentExercise.videoLink,
    description: currentExercise.description
  })

  /**
   * Event handlers:
   */
   
   //Exercise form eventhandler
   const updateExercise = (event) => {
    setExercise({
      ...exercise,
      [event.target.name]: event.target.value,
    })
  }

  // Submit function
  const handleSubmit = (event) => {
    event.preventDefault();
    exerciseSubmit(exercise);
  }


  return (
    <div>
      <Form onSubmit={handleSubmit} className="pb-3 border-bottom">
        <Form.Group>
          <Row>
            <Col md={6}>
              <Form.Label>Exercise Name</Form.Label>
              <Form.Control 
                required
                type="text" 
                placeholder="Enter first name" 
                name="name" 
                onChange={updateExercise}
                maxLength= "20"
                defaultValue={ currentExercise.name ? currentExercise.name : "" } 
              />
            </Col>
            <Col md={6}>
              <Form.Label>Image link</Form.Label>
              <Form.Control 
                type="url" 
                placeholder="Link to a descriptive image" 
                name="imageLocation" 
                onChange={updateExercise}
                defaultValue={ currentExercise.imageLocation ? currentExercise.imageLocation : "" }
              />
            </Col>
          </Row>
          <Row>
          <Col md={6}>
              <Form.Label>Add the target muscle group</Form.Label>
              <Form.Control 
                required
                type="text" 
                placeholder="Muscle group" 
                name="targetMuscleGroup"  
                onChange={updateExercise}
                maxLength= "40"
                defaultValue={ currentExercise.targetMuscleGroup ? currentExercise.targetMuscleGroup : "" }
              />
            </Col>
            <Col md={6}>
              <Form.Label>Video link</Form.Label>
              <Form.Control 
                type="url" 
                placeholder="Link to a descriptive image" 
                name="videoLink" 
                onChange={updateExercise}  
                defaultValue={ currentExercise.videoLink ? currentExercise.videoLink : "" }
              />
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <Form.Label>Description</Form.Label>
              <Form.Control 
                required
                as="textarea"
                rows="3"
                onChange={updateExercise} 
                placeholder="Add an exercise description (max 140 caracters)"
                maxLength= "140"
                name="description"
                defaultValue={ currentExercise.description ? currentExercise.description : "" }
              />
            </Col>
          </Row>
        </Form.Group>
        <Row>
          <Col>
            <Button type="submit" color="primary">Update Exercise</Button>
          </Col>
          <Col className="text-right">
            <Link to="/exercises"><Button color="secondary">Return to exercises</Button></Link>
          </Col>
        </Row>
      </Form>
    </div>
  );
}
export default UpdateExerciseComponent;
import React, { useState } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import FormControl from 'react-bootstrap/FormControl';
import Dropdown from 'react-bootstrap/Dropdown'
import { Link } from 'react-router-dom';

const HomeComponent = ( {isLoading, profiles} ) => {

  const users = profiles.map( profile => {
    return profile.user; 
  })

  const [value, setValue] = useState('');

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Button
      href=""
      ref={ref}
      onClick={e => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
      &#x25bc;
    </Button  >
  ));

  const CustomMenu = React.forwardRef(
    ({ children, style, className }, ref) => {
      return (
        <div
          ref={ref}
          style={style}
          className={className}
        >
          <FormControl
            autoFocus
            className="mx-3 my-2 w-auto"
            placeholder="Type to filter..."
            onChange={e => setValue(e.target.value)}
            value={value}
          />
          <ul className="list-unstyled">
            {
              !value ? (
                <Dropdown.Item eventKey={1} key={1}>Enter a query to see profiles</Dropdown.Item> 
              ) : (
                React.Children.toArray(children).filter(
                  child =>
                    !value || child.props.children.toLowerCase().includes(value),
                ).slice(0,5)
              )
            }
          </ul>
        </div>
      );
    },
  );
  
  return (
    <div>
      <header>
        <h1>Hello!</h1>
        <h3>Welcome to the MyFit appication:</h3>
        <p>Use the NAV-bar, or the buttons below to navigate the page</p>
      </header> 
      <Container className="pt-5">
        <Row>
          <Col md={3}>          
            <Dropdown>
              <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
                Search for profile
              </Dropdown.Toggle>

              <Dropdown.Menu as={CustomMenu}>
                {
                  users.map( (user) => {
                    return (
                      <Dropdown.Item key={user.id} eventKey={user.id} href={ "/profile/" + user.id }>
                        {`${user.firstName} ${user.lastName}`}
                      </Dropdown.Item> 
                    );
                  })
                }
              </Dropdown.Menu>
            </Dropdown>
          </Col>
          <Col md={3}>
            <Link to='/createprofile'><Button>Create a profile here</Button></Link>
          </Col>
          <Col md={3}>
            <Link to='/exercises'><Button>Manage exercises here</Button></Link>
          </Col>
          <Col md={3}>
            <Link to='/goals'><Button>Add a goal here</Button></Link>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default HomeComponent;
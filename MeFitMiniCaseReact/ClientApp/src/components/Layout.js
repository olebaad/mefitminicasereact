import React, { Component } from 'react';
import { Container } from 'reactstrap';
import { NavMenu } from './NavMenu';
import { FootMenu } from './FootMenu';
import './Layout.css';

export class Layout extends Component {
  static displayName = Layout.name;

  render () {
    return (
      <div className='layout'>
        <NavMenu className='position-relative'/>
        <Container className='main-container'> 
          {this.props.children}
        </Container>
        <FootMenu className='position-relative'/>
      </div>
    );
  }
}

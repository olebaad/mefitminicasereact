import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import { Row, Col } from 'reactstrap';

const UpdateProfileComponent = ( {currentProfile, profileSubmit, programs, workouts, goals} ) => {
  //Declare check values for radio buttons 
  let isAdminYes;
  let isAdminNo;  
  let isContributorYes;
  let isContributorNo;

  if (currentProfile.user.isContributor === true){
    isContributorYes = true;
    isContributorNo = false; 
  } else {
    isContributorYes = false;
    isContributorNo = true; 
  }  

  if (currentProfile.user.isAdmin === true){
    isAdminYes = true;
    isAdminNo = false; 
  } else {
    isAdminYes = false;
    isAdminNo = true; 
  }

  //Add a profile object to state
  const [profile, setProfile] = useState({
    id: currentProfile.id,
    user: currentProfile.user,
    address: currentProfile.address,
    goalId: currentProfile.goalId,
    programId: currentProfile.programId,
    workoutId: currentProfile.workoutId,
    medicalConditions: currentProfile.medicalConditions,
    disabilities: currentProfile.disabilities,
    weightInKg: currentProfile.weightInKg,
    heightInCm: currentProfile.heightInCm
  })

  /**
   * Event handlers:
   */
   
  //Custom handler for postalcodes
  const handlePostalChange = (event) => {    
    const reg = new RegExp("^[0-9]{1,4}$")
    if(!reg.test(event.target.value)) {
      if (event.target.value.length > 1) {
        event.target.value = event.target.value.substr(0, event.target.value.length-1);
      } else {
        event.target.value = "";
      }
    } else {
      const value = parseInt(event.target.value, 10)
      setProfile({
        ...profile,
        address: {
          ...profile.address,
          [event.target.name]: value,
        }
      })
    }
  }

  //User form eventhandler
  const updateUser = (event) => {
    let value = event.target.value;
    if(event.target.type === "radio" && event.target.checked) {
      value = JSON.parse(event.target.value)
    } 
    else if (event.target.type === "radio" && !event.target.checked) {
      return;
    }

    setProfile({
      ...profile,
      user: {
        ...profile.user,
        [event.target.name]: value,
      }
    })
  }
  
  //Eventhandler for form elements that are of basic types
  const updateConstants = (event) => {
    setProfile({
      ...profile,
      [event.target.name]: event.target.value
    })
  }

  //Address form eventhandler
  const updateAddress = (event) => {
    setProfile({
      ...profile,
      address: {
        ...profile.address,
        [event.target.name]: event.target.value,
      }
    })
  }

  //Droprown menues eventhandler
  const updateDropdowns = (event) => {
    let value;
    if(event.target.value === "Choose your Workout") {
      value = 0;
    } else {
      value = parseInt(event.target.value);
    }
    setProfile({
      ...profile,
      [event.target.name + "Id"]: value
    })
  }

  //Function for validation of postalcodes
  const validPostalCode = (postalCode) => {
    if (postalCode.toString().length === 4 || postalCode === 0){
      return true;
    } else {
      return false;
    }
  }
  // Submit function
  const handleSubmit = (event) => {
    event.preventDefault();

    if(!validPostalCode(profile.address.postalCode)){
      alert("Postal code must be 4 digits!")
    } else {
      profileSubmit(profile);
    }
  }


  return (
    <div>
      <Form onSubmit={handleSubmit} className="pb-3">
        <Form.Group>
          <Row>
            <Col md={6}>
              <Form.Label>First name</Form.Label>
              <Form.Control 
                required
                type="text" 
                placeholder="Enter first name" 
                name="firstName" 
                defaultValue={ currentProfile.user.firstName ? currentProfile.user.firstName : "" } 
                onChange={updateUser}
              />
            </Col>
            <Col md={6}>
              <Form.Label>Last name</Form.Label>
              <Form.Control 
                required
                type="text" 
                placeholder="Enter last name" 
                name="lastName" 
                defaultValue={ currentProfile.user.lastName ? currentProfile.user.lastName : "" } 
                onChange={updateUser}
              />              
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <Form.Label>Password: </Form.Label>
              <Form.Control type="password" placeholder="Password" name="password" onChange={updateUser} />
            </Col>
            <Col md={3}>
              <Form.Check>
                <Form.Label>Contributor</Form.Label>
                <Form.Check label="Yes" name="isContributor" type="radio" value={true} onChange={updateUser} defaultChecked={isContributorYes}/>
                <Form.Check label="No" name="isContributor" type="radio" value={false} onChange={updateUser} defaultChecked={isContributorNo}/>
              </Form.Check>
            </Col>
            <Col md={3}>
              <Form.Check>
                <Form.Label>Admin</Form.Label>
                <Form.Check label="Yes"  name="isAdmin"type="radio" value={true} onChange={updateUser} defaultChecked={isAdminYes}/>
                <Form.Check label="No" name="isAdmin" type="radio" value={false} onChange={updateUser} defaultChecked={isAdminNo}/>
              </Form.Check>
            </Col>
          </Row>
        </Form.Group>
        <Form.Group className="border-top  pt-3">
          <Row>
            <Col md={4}>
              <Form.Label>Medical Conditions</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Medical Conditions" 
                name="medicalConditions" 
                defaultValue={ currentProfile.medicalConditions ? currentProfile.medicalConditions : "" } 
                onChange={updateConstants}  
              />
            </Col>
            <Col md={4}>
              <Form.Label>Disabilities</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Disabilities" 
                name="disabilities" 
                defaultValue={ currentProfile.disabilities ? currentProfile.disabilities : "" } 
                onChange={updateConstants}  
              />
            </Col>
            <Col md={2}>
              <Form.Label>Weight (Kg)</Form.Label>
              <Form.Control 
                type="number" 
                placeholder="Weight in Kg" 
                name="weightInKg" 
                defaultValue={ currentProfile.weightInKg ? currentProfile.weightInKg : "" } 
                onChange={updateConstants}  
              />
            </Col>
            <Col md={2}>
              <Form.Label>Height (cm)</Form.Label>
              <Form.Control 
                type="number" 
                placeholder="Height in cm" 
                name="heightInCm" 
                defaultValue={ currentProfile.heightInCm ? currentProfile.heightInCm : "" } 
                onChange={updateConstants}  
              />
            </Col>
          </Row>
        </Form.Group>
        <Form.Group className="border-top pt-3">
          <Row>
            <Col md={6}>
              <Form.Label>Address Line 1</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Address" 
                name="addressLine1" 
                defaultValue={ currentProfile.address.addressLine1 ? currentProfile.address.addressLine1 : "" } 
                onChange={updateAddress}  
              />
            </Col>
            <Col md={6}>
              <Form.Label>City</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="City" 
                name="city" 
                defaultValue={ currentProfile.address.city ? currentProfile.address.city : "" } 
                onChange={updateAddress}  
              />

            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <Form.Label>Address Line 2</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Address" 
                name="addressLine2" 
                defaultValue={ currentProfile.address.addressLine2 ? currentProfile.address.addressLine2 : "" } 
                onChange={updateAddress}  
              />              
            </Col>
            <Col md={6}>
            <Form.Label>Country</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Country" 
                name="country" 
                defaultValue={ currentProfile.address.country ? currentProfile.address.country : "" }
                onChange={updateAddress}  
              />
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <Form.Label>Address Line 3</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Address" 
                name="addressLine3" 
                defaultValue={ currentProfile.address.addressLine3 ? currentProfile.address.addressLine3 : "" } 
                onChange={updateAddress}  
              />              
            </Col>
            <Col md={6}>
              <Form.Label>Postal Code</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Postal code" 
                name="postalCode" 
                defaultValue={ currentProfile.address.postalCode ? currentProfile.address.postalCode : "" } 
                onChange={handlePostalChange}
              />
            </Col>
          </Row> 
        </Form.Group>
        <Form.Group className="border-top pt-3 pb-5">
          <Row>
            <Col md={3}>
              <Form.Label>Goal</Form.Label>
              <select name="goal" onChange={updateDropdowns} defaultValue={ currentProfile.goal ? currentProfile.goal.id : 0 } className="browser-default custom-select">
                <option>Choose your Goal</option>
                {
                  goals.map( (goal) => {
                    return (
                      <option key={goal.id} value={goal.id}>
                        {goal.type}
                      </option> 
                    );
                  })
                }
              </select>
            </Col>
            <Col md={4}>
              <Form.Label>Workout</Form.Label>
              <select name="workout" onChange={updateDropdowns} defaultValue={ currentProfile.workout ? currentProfile.workout.id : 0 } className="browser-default custom-select">
                <option>Choose your Workout</option>
                {
                  workouts.map( (workout) => {
                    return (
                      <option key={workout.id} value={workout.id}>
                        {workout.name}: {workout.type}
                      </option> 
                    );
                  })
                }
              </select>
            </Col>
            <Col md={5}>
              <Form.Label>Program</Form.Label>
              <select name="Program" onChange={updateDropdowns} defaultValue={ currentProfile.program ? currentProfile.program.id : 0 } className="browser-default custom-select">
                <option>Choose your Program</option>
                {
                  programs.map( (program) => {
                    return (
                      <option key={program.id} value={program.id}>
                        {program.name}: {program.category}
                      </option> 
                    );
                  })
                }
              </select>
            </Col>
          </Row>
        </Form.Group>
        <Button type="submit">Update Profile</Button>
      </Form>
    </div>
  );
}
export default UpdateProfileComponent;